 /*link_walker.c
 * This file is part of Link walker
 *
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * Link walker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Link walker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Link walker; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

 /* ------------
 * NOTICE
 * -------------
 * This is just for SDL2 didactic demostration 
 */ 
	
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#define WINDOW_W 640
#define WINDOW_H 480 
#define FPS (1000/25)
#define DATA_DIR "../data/"

enum{
	IMG_LINK1,
	IMG_LINK2,
	IMG_LINK3,
	IMG_LINK4,
	IMG_LINK5,
	IMG_LINK6,
	IMG_LINK7,
	IMG_LINK8,

	NUM_IMAGES
};

// Game states 
enum{
	PLAYING,
	STOP,
	EXIT
};

// Link directions
enum{
	DOWN,
	LEFT,
	UP,
	RIGHT
};

// Link states
enum{
	STANDING,
	WALKING
	//TODO: Attack
};

const char *names_images [NUM_IMAGES]={
	DATA_DIR "images/Link 1.gif",
	DATA_DIR "images/Link 2.gif",
	DATA_DIR "images/Link 3.gif",
	DATA_DIR "images/Link 4.gif",
	DATA_DIR "images/Link 5.gif",
	DATA_DIR "images/Link 6.gif",
	DATA_DIR "images/Link 7.gif",
	DATA_DIR "images/Link 8.gif"
}; 

SDL_Surface *images[NUM_IMAGES];

SDL_Window * window;
SDL_Surface *screen;

typedef struct _Link
{
	int x;
	int y;
	int state;
	int speed;
} Link;

Link myLink;

void move_link(int direction)
{
	switch(direction)
	{
		case UP:
			myLink.y -=myLink.speed;
		break;

		case DOWN:
			myLink.y +=myLink.speed;
		break;

		case LEFT:
			myLink.x -=myLink.speed;
		break;

		case RIGHT:
			myLink.x +=myLink.speed;
		break;
	};
}

int draw_all(int frame, int direction)
{
	SDL_Rect rect;

	// Clear screen
	SDL_Rect rect_gb;
	rect_gb.w = WINDOW_W;
	rect_gb.h = WINDOW_H;
	SDL_FillRect (screen, &rect_gb, 0);

	// Ajust link position
	rect.x = myLink.x;
	rect.y = myLink.y;
	rect.w = images[(frame / 8) + (direction * 2)]->w;
	rect.h = images[(frame / 8) + (direction * 2)]->h;

	SDL_BlitSurface(images[(frame / 8) + (direction * 2)],NULL, screen,&rect);

	return 0;
}

void init_mylink()
{
	myLink.x = WINDOW_W / 2;
	myLink.y = WINDOW_H / 2;
	myLink.state = STANDING;
	myLink.speed = 20;
}

int setup()
{
	int e;

// Init the video 
	if (SDL_Init (SDL_INIT_VIDEO) < 0) {
		printf ("\nError SDL_Init.\n");
		return 1;
	}
 	window = SDL_CreateWindow("Link walker", 10, 10, 640, 480, 0);
 	screen = SDL_GetWindowSurface(window);

// Load images
 	for (e = 0; e < NUM_IMAGES; e++)
 	{
 		images[e] = IMG_Load(names_images[e]);
 		if (images[e] == NULL)
 		 {
 		 	printf("\nImage error load: %s", names_images[e]);
 		 	return 1;
 		 } 
 	}

// Init myLInk
 	init_mylink();

return 0;
}

int main(int argc, char *argv[])
{
	int game_status = PLAYING;
	int last_time, now_time;
	SDL_Event event;
	int frame = 0;
	int direction = DOWN;

	if (setup()){ printf("\n Error setup.");}

	do
	{
	last_time = SDL_GetTicks();

	// Listen event 
	while(SDL_PollEvent(&event) > 0)
	{
		switch(event.type){
			case SDL_QUIT:
				game_status = EXIT;
			break;

			case SDL_KEYDOWN:
				// Check the direction of the link 
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						direction = UP;
						move_link(UP);
					break;

					case SDLK_DOWN:
					  	direction = DOWN;
						move_link(DOWN);
					break;

					case SDLK_LEFT:
					 	direction = LEFT;
						move_link(LEFT);
					break;

					case SDLK_RIGHT:
					   	direction = RIGHT;
						move_link(RIGHT);
					break;
				// ESC button
					case SDLK_ESCAPE:
						game_status = EXIT;
					break;
				}
			break;
		}
	}
		// Draw all 
		draw_all(frame, direction);

		// ++ and Check frame 
		frame++;
		if (frame > 15){frame = 0;}

		// Flip/Update window
		SDL_UpdateWindowSurface(window);

		// Check game speed
 		now_time = SDL_GetTicks();

		if (now_time < last_time + FPS) {
			SDL_Delay(last_time + FPS - now_time);
		}
 	} while (game_status != EXIT);

 	return 0;
 }